package joc;


import carti.Deck;
import carti.DiscardPile;
import carti.Hand;


public abstract class Gamer {
	protected Hand hand = new Hand();
	protected boolean knock = false;
	protected int points;
	
	public Gamer(){
		this.points = 0;
	}
	public Hand getHand(){
		return this.hand;
	}
	public void setHand(Hand hand){
		this.hand = hand;
	}
	public boolean getKnock(){
		return this.knock;
	}
	public void setKnock(boolean knock){
		this.knock = knock;
	}
	public int getPoints(){
		return this.points;
	}
	public void setPoints(int points){
		this.points += points;
	}
	public abstract void draw(Deck deck , DiscardPile discardPile , boolean trageCarte);
	public abstract void discard(DiscardPile discardPile , int carteAleasa);
	public boolean canIKnock(){
		int sumDW = 0;
		int maxDW = 0;
		for (int i=0;i<hand.getSize() ; i++){
			if (hand.cardByPosition(i).rank() > maxDW && hand.cardByPosition(i).getDeadwood() == true)
				maxDW = hand.cardByPosition(i).rank();
			if (hand.cardByPosition(i).getDeadwood() == true)
			sumDW += hand.cardByPosition(i).rank();
		}
		if ( sumDW - maxDW <= 10)
			return true;
		return false;
	}
	public void discardAll() throws Exception{
		while(hand.getSize() > 0)
			hand.removeCard(hand.cardByPosition(0));
	}
	
	public int getTotalPoints(){
		int sum = 0;
		for (int i = 0;i<hand.getSize() ; i++)
			if (hand.cardByPosition(i).getDeadwood() == true)
				sum += hand.cardByPosition(i).rank();
		return sum;
		
	}
	public int knockCard(){
		int knk = 0;
		for (int i=0;i<hand.getSize();i++)
			if (hand.cardByPosition(i).getDeadwood() == true && hand.cardByPosition(i).rank() > knk)
				knk = hand.cardByPosition(i).rank();
		return knk;
	}
	public void pair(){
		Hand hand1 = new Hand(hand);
		hand1.resetCards();
		hand1.sortBySuit();
		int i=0;
		int k,j;
		while (i<hand1.getSize() - 2){
			k=0;
			j=i;
			while (hand1.cardByPosition(j).getSuit() == hand1.cardByPosition(j+1).getSuit() && hand1.cardByPosition(j+1).getValue() - hand1.cardByPosition(j).getValue() == 1 && j < hand1.getSize() - 1){
				k++;
				j++;
				if (j == hand1.getSize() - 1)
					break;
			}
			if (k>=2){
				for (int contor = i ; contor <= j ;contor++){
					hand1.cardByPosition(contor).setDeadwood(false);
					hand1.cardByPosition(contor).setRun(true);
				}
				i=j+1;
			}
			else i++;
		}
		hand1.sortByValue();
		i=0;
		while (i<hand1.getSize() - 2){
			k=0;
			j=i;
			while (hand1.cardByPosition(j+1).getValue() == hand1.cardByPosition(j).getValue() && j < hand1.getSize() - 1 && hand1.cardByPosition(j).getRun() == false && hand1.cardByPosition(j+1).getRun() == false){
				k++;
				j++;
				if (j == hand1.getSize() - 1)
					break;
			}
			if (k>=2){
				for (int contor = i ; contor <= j ;contor++){
					hand1.cardByPosition(contor).setDeadwood(false);
					hand1.cardByPosition(contor).setSet(true);
				}
				i=j+1;
			}
			else i++;
		}
	}
}
