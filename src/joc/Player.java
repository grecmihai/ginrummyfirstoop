package joc;

import carti.*;
public class Player extends Gamer{
	public Player(){
		super();
	}
	
	
	public void draw(Deck deck , DiscardPile discardPile , boolean trageCarte){
		if (trageCarte)
		hand.addCard(deck.dealCard());
		
		
		else
			hand.addCard(discardPile.getTop());
	}
		
	
	public void discard(DiscardPile discardPile , int carteAleasa){
		discardPile.setTop(hand.cardByPosition(carteAleasa));
		hand.removeCard(hand.cardByPosition(carteAleasa));
		
	}
	
	
}
