package joc;
import carti.*;
public class Computer extends Gamer{
	public Computer(){
		super();
	}
	public void draw(Deck deck , DiscardPile discardPile , boolean trageCarte){
		int k=0;
		int j=0;
		for (int i=0;i<hand.getSize();i++)
			if (hand.cardByPosition(i).getValue() == discardPile.getTop().getValue())
				k++;
		for (int i=0;i<hand.getSize();i++)
			if (hand.cardByPosition(i).getSuit() == discardPile.getTop().getSuit() && Math.abs(hand.cardByPosition(i).getValue() - discardPile.getTop().getValue()) == 1)
				j++;
		if (k>=2 || j>=2)
			hand.addCard(discardPile.getTop());
		else
			hand.addCard(deck.dealCard());
		
	}
	public void discard(DiscardPile discardPile , int carteAleasa){
		int pos = 0;
		for (int i=0;i<hand.getSize() - 1;i++)
			if (hand.cardByPosition(i).getDeadwood() == true){
				pos = i;
				break;
			}
		discardPile.setTop(hand.cardByPosition(pos));
		hand.removeCard(hand.cardByPosition(pos));
		
	}
}
