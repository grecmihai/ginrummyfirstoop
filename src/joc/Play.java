package joc;

import carti.*;
public class Play {
		
		public Player player1 = new Player();
		public Computer player2 = new Computer();
		public Deck deck = new Deck();
		public DiscardPile discardPile = new DiscardPile();
		
		public Play(){}

	public void addPoints(){
		if (player1.canIKnock())
			if (player1.getTotalPoints() - player1.knockCard() > player2.getTotalPoints())
				player2.setPoints(25 - player2.getTotalPoints() + player1.getTotalPoints() - player1.knockCard());
			else 
				player1.setPoints(player2.getTotalPoints() - player1.getTotalPoints() + player1.knockCard());
		else if (player2.canIKnock())
			if (player1.getTotalPoints() < player2.getTotalPoints() - player2.knockCard())
				player1.setPoints(25 - player1.getTotalPoints() + player2.getTotalPoints() - player2.knockCard());
			else 
				player2.setPoints(player1.getTotalPoints() - player2.getTotalPoints() + player2.knockCard());
	}
}
	
		
