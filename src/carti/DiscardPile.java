package carti;

public class DiscardPile {
	Card card;
	
	public DiscardPile(){
		
	}
	
	public void setTop(Card card){
		this.card = card;
	}
	public Card getTop(){
		return this.card;
	}
	public void showDiscardPile(){
		this.card.showCard();
	}
	
	
}
