package carti;

import java.util.Comparator;

public class CompareByValue implements Comparator<Card>{
	@Override
	public int compare(Card card1 , Card card2){
		int result;
		result =  card1.getValue() - card2.getValue();
		if (result == 0)
			return card1.getSuit() - card2.getSuit();
		else return result;
	}
}
