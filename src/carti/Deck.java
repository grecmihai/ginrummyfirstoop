package carti;

import java.util.ArrayList;

public class Deck {
	
	private int[] suits = {Constants.SPADES , Constants.HEARTS , Constants.CLUBS , Constants.DIAMONDS};
	private int[] ranks = {Constants.ACE , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , Constants.JACK , Constants.QUEEN , Constants.KING};
	private ArrayList<Card> cards;
	
	public Deck(){
		cards = new ArrayList<Card>();
		for (int i=0 ; i<suits.length ; i++){
			for (int j=0 ; j<ranks.length ; j++){
				cards.add(new Card(ranks[j] , suits[i]));
			}
		}
		this.shuffle();
	}
	public ArrayList<Card> getDeckCards(){
		return this.cards;
	}
	public void shuffle(){
		ArrayList<Card> temp = new ArrayList<Card>();
		while (!cards.isEmpty()){
			int loc = (int)(Math.random()*cards.size());
			temp.add(cards.get(loc));
			cards.remove(loc);
		}
		cards = temp;
	}
	
	public void showcards(){
		for (int i=0;i<cards.size();i++){
			cards.get(i).showCard();

		}
	}
	
	public Card dealCard(){
		Card card = new Card();
			card = cards.get(0);
			cards.remove(0);
			return card;
		
	}
	
}
