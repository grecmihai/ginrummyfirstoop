package carti;

import java.util.Comparator;

public class CompareBySuit implements Comparator<Card>{
	@Override
	public int compare(Card card1 , Card card2){
		int result;
		result =  card1.getSuit() - card2.getSuit();
		if (result == 0)
			return card1.getValue() - card2.getValue();
		else return result;
	}
}
