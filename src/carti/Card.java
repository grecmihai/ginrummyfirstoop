package carti;

public class Card implements Constants{
	private final int  value;
	private final int  suit;
	private boolean set = false;
	private boolean run = false;
	private boolean deadwood = true;
	
	
	public Card(){
		this.value = 1;
		this.suit = 1;
	}
	
	public Card(int  pValue , int  pSuit){
		if (pSuit != Constants.SPADES && pSuit!=Constants.HEARTS && pSuit!=Constants.DIAMONDS && pSuit!=Constants.CLUBS)
			throw new IllegalArgumentException("Illegal playing card suit");
		if (pValue < 1 || pValue>13)
			throw new IllegalArgumentException("Illegal playing card value");
		this.value = pValue;
		this.suit = pSuit;
	}
	
	public boolean getSet(){
		return this.set;
	}
	public boolean getRun(){
		return this.run;
	}
	public boolean getDeadwood(){
		return this.deadwood;
	}
	public void setSet(boolean set){
		this.set = set;
	}
	public void setRun(boolean run){
		this.run = run;
	}
	public void setDeadwood(boolean deadwood){
		this.deadwood = deadwood;
	}
	
	public int  getSuit(){
		return suit;
	}
	public int  getValue(){
		return value;
	}
	public String getSuitAsString(){
		switch (suit){
		case Constants.CLUBS: return "Clubs";
		case Constants.DIAMONDS: return "Diamonds";
		case Constants.HEARTS: return "Hearts";
		case Constants.SPADES: return "Spades";
		default: throw new IllegalArgumentException("Illegal playing card suit");

		}
		
	}
	
	public String getValueAsString(){
		switch (value){
		case 1:   return "Ace"; 
        case 2:   return "2";
        case 3:   return "3";
        case 4:   return "4";
        case 5:   return "5";
        case 6:   return "6";
        case 7:   return "7";
        case 8:   return "8";
        case 9:   return "9";
        case 10:  return "10";
        case 11:  return "Jack";
        case 12:  return "Queen";
        case 13:  return "King";
        default:  throw new IllegalArgumentException("Illegal playing card value");

		}
	}
	
	public void showCard(){
		System.out.println(getValueAsString() + " of " + getSuitAsString());
	}
	
	public int rank(){
		switch (this.value){
		case Constants.ACE: return 1;
		case 2: return 2;
		case 3: return 3;
		case 4: return 4;
		case 5: return 5;
		case 6: return 6;
		case 7: return 7;
		case 8: return 8;
		case 9: return 9;
		case 10: return 10;
		case Constants.JACK: return 10;
		case Constants.QUEEN: return 10;
		case Constants.KING: return 10;
		default:return 0;

		}
	}
}
