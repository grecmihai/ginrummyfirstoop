package carti;
import java.util.ArrayList;
import java.util.Collections;

public class Hand {
	private ArrayList<Card> cards;
	
	public Hand(){
		cards = new ArrayList<Card>(11);
	}
	public Hand (Hand another){
		this.cards = new ArrayList<>(another.cards);
	}
	public ArrayList<Card> getHandCards(){
		return this.cards;
	}
	public void addCard(Card card){
		if (cards.size() < 11)
		cards.add(card);
	}
	public void removeCard(Card card) {
		int test = 0;
		for (int i=0 ; i<cards.size() ; i++)
			if (card.getValue() == cards.get(i).getValue() && card.getSuit() == cards.get(i).getSuit())
				test = 1;
		if (test==1)
			cards.remove(card);
		
	}
	public int getSize(){
		return cards.size();
	}
	
	public void showHand(){
		for (int i=0 ; i<cards.size() ; i++)
			cards.get(i).showCard();
	}
	
	public void sortByValue(){
		Collections.sort(cards , new CompareByValue());
	}
	public void sortBySuit(){
		Collections.sort(cards , new CompareBySuit());
	}
	public Card cardByPosition(int i){
		return cards.get(i);
	}
	public void resetCards(){
		for (int i=0;i<cards.size();i++){
			cards.get(i).setDeadwood(true);
			cards.get(i).setRun(false);
			cards.get(i).setSet(false);
		}
	}
	public void removeAllCards(){
		cards.clear();
	}
	
	
}
