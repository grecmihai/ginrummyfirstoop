package view;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class Table extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel[] playerCard;
	private JLabel[] computerCard;
	private JLabel deckImage , space , discardImage;
	private JLabel text;
	private JButton deal , sortByS , sortByV;
	private final Color color= new Color(0 , 94 , 0);
	private CardView images = new CardView();
	private boolean allowDeal =true;
	private boolean	buttonPermission = true;
	
	public Table(){
		super("Gin Rummy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		playerCard = new JLabel[11];
		playerCard[0] = new JLabel(images.getBlank());
		playerCard[1] = new JLabel(images.getBlank());
		playerCard[2] = new JLabel(images.getBlank());
		playerCard[3] = new JLabel(images.getBlank());
		playerCard[4] = new JLabel(images.getBlank());
		playerCard[5] = new JLabel(images.getBlank());
		playerCard[6] = new JLabel(images.getBlank());
		playerCard[7] = new JLabel(images.getBlank());
		playerCard[8] = new JLabel(images.getBlank());
		playerCard[9] = new JLabel(images.getBlank());
		playerCard[10] = new JLabel(images.getBlank());
		
		computerCard = new JLabel[11];
		computerCard[0] = new JLabel(images.getBlank());
		computerCard[1] = new JLabel(images.getBlank());
		computerCard[2] = new JLabel(images.getBlank());
		computerCard[3] = new JLabel(images.getBlank());
		computerCard[4] = new JLabel(images.getBlank());
		computerCard[5] = new JLabel(images.getBlank());
		computerCard[6] = new JLabel(images.getBlank());
		computerCard[7] = new JLabel(images.getBlank());
		computerCard[8] = new JLabel(images.getBlank());
		computerCard[9] = new JLabel(images.getBlank());
		computerCard[10] = new JLabel(images.getBlank());
		
		deckImage = new JLabel(images.getBack());
		space = new JLabel(images.getBlank());
		discardImage = new JLabel(images.getBlank());
		
		deal = new JButton("DEAL");
		sortByS = new JButton("SORT BY SUIT");
		sortByV = new JButton("SORT BY VALUE");
		
		JPanel playerPanel = new JPanel();
		playerPanel.setLayout(new FlowLayout());
		playerPanel.setBackground(color);
		playerPanel.add(playerCard[0]);
		playerPanel.add(playerCard[1]);
		playerPanel.add(playerCard[2]);
		playerPanel.add(playerCard[3]);
		playerPanel.add(playerCard[4]);
		playerPanel.add(playerCard[5]);
		playerPanel.add(playerCard[6]);
		playerPanel.add(playerCard[7]);
		playerPanel.add(playerCard[8]);
		playerPanel.add(playerCard[9]);
		playerPanel.add(playerCard[10]);
		
		JPanel computerPanel = new JPanel();
		computerPanel.setLayout(new FlowLayout());
		computerPanel.setBackground(color);
		computerPanel.add(computerCard[0]);
		computerPanel.add(computerCard[1]);
		computerPanel.add(computerCard[2]);
		computerPanel.add(computerCard[3]);
		computerPanel.add(computerCard[4]);
		computerPanel.add(computerCard[5]);
		computerPanel.add(computerCard[6]);
		computerPanel.add(computerCard[7]);
		computerPanel.add(computerCard[8]);
		computerPanel.add(computerCard[9]);
		computerPanel.add(computerCard[10]);
		
		JPanel deckdiscard = new JPanel();
		deckdiscard.setLayout(new FlowLayout());
		deckdiscard.setBackground(color);
		deckdiscard.add(deckImage);
		deckdiscard.add(space);
		deckdiscard.add(discardImage);
		
		JPanel center = new JPanel();
		center.setLayout(new BoxLayout(center , BoxLayout.Y_AXIS));
		center.setBackground(color);
		center.add(Box.createRigidArea(new Dimension(0 , 250)));
		center.add(deckdiscard);
		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons , BoxLayout.X_AXIS));
		buttons.setBackground(color);
		buttons.add(Box.createRigidArea(new Dimension(100 , 0)));
		buttons.add(deal);
		buttons.add(Box.createRigidArea(new Dimension(50,0)));
		buttons.add(sortByS);
		buttons.add(Box.createRigidArea(new Dimension(50 , 0)));
		buttons.add(sortByV);
		center.add(buttons);
		text = new JLabel("Bine ati venit!Va uram succes.");
		text.setFont(new Font(text.getFont().getName() , Font.BOLD , text.getFont().getSize() + 10));
		text.setForeground(Color.black);
		center.add(Box.createRigidArea(new Dimension(0 , 25)));
		center.add(text);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBackground(color);
		panel.add(playerPanel , BorderLayout.SOUTH);
		panel.add(computerPanel , BorderLayout.NORTH);
		panel.add(center , BorderLayout.CENTER);
		
		setContentPane(panel);
		
	}

	
	
	public void addCardsListener(MouseListener handler1){
		playerCard[0].addMouseListener(handler1);
		playerCard[1].addMouseListener(handler1);
		playerCard[2].addMouseListener(handler1);
		playerCard[3].addMouseListener(handler1);
		playerCard[4].addMouseListener(handler1);
		playerCard[5].addMouseListener(handler1);
		playerCard[6].addMouseListener(handler1);
		playerCard[7].addMouseListener(handler1);
		playerCard[8].addMouseListener(handler1);
		playerCard[9].addMouseListener(handler1);
		playerCard[10].addMouseListener(handler1);
	}
	
	public void addDeckDiscardListener(MouseListener handler2){
		deckImage.addMouseListener(handler2);
		discardImage.addMouseListener(handler2);
	}
	
	public void addDealListener(ActionListener listener){
		deal.addActionListener(listener);
	}
	
	public void addSortListener(ActionListener listener){
		sortByS.addActionListener(listener);
		sortByV.addActionListener(listener);
	}
	
	public void setDiscardImage(ImageIcon image){
		this.discardImage.setIcon(image);
	}
	public void setPlayerImage(ImageIcon image , int i){
		this.playerCard[i].setIcon(image);
	}
	public void setComputerImage(ImageIcon image , int i){
		this.computerCard[i].setIcon(image);
	}
	public void setHelpText(String text){
		this.text.setText(text);
	}
	
	public JLabel getDiscardImage(){
		return discardImage;
	}
	public JLabel getDeckImage(){
		return deckImage;
	}
	public JLabel getPlayerImage(int i){
		return playerCard[i];
	}
	public JLabel getComputerImage(int i){
		return computerCard[i];
	}

	public JButton getSortByS() {
		return sortByS;
	}

	public JButton getSortByV() {
		return sortByV;
	}



	public boolean isAllowDeal() {
		return allowDeal;
	}



	public void setAllowDeal(boolean allowDeal) {
		this.allowDeal = allowDeal;
	}



	public boolean isButtonPermission() {
		return buttonPermission;
	}



	public void setButtonPermission(boolean buttonPermission) {
		this.buttonPermission = buttonPermission;
	}
	
	
	
	
	
}
