package view;
import javax.swing.*;
import carti.Constants;
import carti.Card;

public class CardView {
	
	private final ImageIcon tenClubs = new ImageIcon(getClass().getResource("10_of_clubs.png"));
	private final ImageIcon tenDiamonds = new ImageIcon(getClass().getResource("10_of_diamonds.png"));
	private final ImageIcon tenHearts = new ImageIcon(getClass().getResource("10_of_hearts.png"));
	private final ImageIcon tenSpades = new ImageIcon(getClass().getResource("10_of_spades.png"));
	private final ImageIcon twoClubs = new ImageIcon(getClass().getResource("2_of_clubs.png"));
	private final ImageIcon twoDiamonds = new ImageIcon(getClass().getResource("2_of_diamonds.png"));
	private final ImageIcon twoHearts = new ImageIcon(getClass().getResource("2_of_hearts.png"));
	private final ImageIcon twoSpades = new ImageIcon(getClass().getResource("2_of_spades.png"));
	private final ImageIcon threeClubs = new ImageIcon(getClass().getResource("3_of_clubs.png"));
	private final ImageIcon threeDiamonds = new ImageIcon(getClass().getResource("3_of_diamonds.png"));
	private final ImageIcon threeHearts = new ImageIcon(getClass().getResource("3_of_hearts.png"));
	private final ImageIcon threeSpades = new ImageIcon(getClass().getResource("3_of_spades.png"));
	private final ImageIcon fourClubs = new ImageIcon(getClass().getResource("4_of_clubs.png"));
	private final ImageIcon fourDiamonds = new ImageIcon(getClass().getResource("4_of_diamonds.png"));
	private final ImageIcon fourHearts = new ImageIcon(getClass().getResource("4_of_hearts.png"));
	private final ImageIcon fourSpades = new ImageIcon(getClass().getResource("4_of_spades.png"));
	private final ImageIcon fiveClubs = new ImageIcon(getClass().getResource("5_of_clubs.png"));
	private final ImageIcon fiveDiamonds = new ImageIcon(getClass().getResource("5_of_diamonds.png"));
	private final ImageIcon fiveHearts = new ImageIcon(getClass().getResource("5_of_hearts.png"));
	private final ImageIcon fiveSpades = new ImageIcon(getClass().getResource("5_of_spades.png"));
	private final ImageIcon sixClubs = new ImageIcon(getClass().getResource("6_of_clubs.png"));
	private final ImageIcon sixDiamonds = new ImageIcon(getClass().getResource("6_of_diamonds.png"));
	private final ImageIcon sixHearts = new ImageIcon(getClass().getResource("6_of_hearts.png"));
	private final ImageIcon sixSpades = new ImageIcon(getClass().getResource("6_of_spades.png"));
	private final ImageIcon sevenClubs = new ImageIcon(getClass().getResource("7_of_clubs.png"));
	private final ImageIcon sevenDiamonds = new ImageIcon(getClass().getResource("7_of_diamonds.png"));
	private final ImageIcon sevenHearts = new ImageIcon(getClass().getResource("7_of_hearts.png"));
	private final ImageIcon sevenSpades = new ImageIcon(getClass().getResource("7_of_spades.png"));
	private final ImageIcon eightClubs = new ImageIcon(getClass().getResource("8_of_clubs.png"));
	private final ImageIcon eightDiamonds = new ImageIcon(getClass().getResource("8_of_diamonds.png"));
	private final ImageIcon eightHearts = new ImageIcon(getClass().getResource("8_of_hearts.png"));
	private final ImageIcon eightSpades = new ImageIcon(getClass().getResource("8_of_spades.png"));
	private final ImageIcon nineClubs = new ImageIcon(getClass().getResource("9_of_clubs.png"));
	private final ImageIcon nineDiamonds = new ImageIcon(getClass().getResource("9_of_diamonds.png"));
	private final ImageIcon nineHearts = new ImageIcon(getClass().getResource("9_of_hearts.png"));
	private final ImageIcon nineSpades = new ImageIcon(getClass().getResource("9_of_spades.png"));
	private final ImageIcon aceClubs = new ImageIcon(getClass().getResource("ace_of_clubs.png"));
	private final ImageIcon aceDiamonds = new ImageIcon(getClass().getResource("ace_of_diamonds.png"));
	private final ImageIcon aceHearts = new ImageIcon(getClass().getResource("ace_of_hearts.png"));
	private final ImageIcon aceSpades = new ImageIcon(getClass().getResource("ace_of_spades2.png"));
	private final ImageIcon back = new ImageIcon(getClass().getResource("back.png"));
	private final ImageIcon blank = new ImageIcon(getClass().getResource("blank.png"));
	private final ImageIcon jackClubs = new ImageIcon(getClass().getResource("jack_of_clubs2.png"));
	private final ImageIcon jackDiamonds = new ImageIcon(getClass().getResource("jack_of_diamonds2.png"));
	private final ImageIcon jackHearts = new ImageIcon(getClass().getResource("jack_of_hearts2.png"));
	private final ImageIcon jackSpades = new ImageIcon(getClass().getResource("jack_of_spades2.png"));
	private final ImageIcon kingClubs = new ImageIcon(getClass().getResource("king_of_clubs2.png"));
	private final ImageIcon kingDiamonds = new ImageIcon(getClass().getResource("king_of_diamonds2.png"));
	private final ImageIcon kingHearts = new ImageIcon(getClass().getResource("king_of_hearts2.png"));
	private final ImageIcon kingSpades = new ImageIcon(getClass().getResource("king_of_spades2.png"));
	private final ImageIcon queenClubs = new ImageIcon(getClass().getResource("queen_of_clubs2.png"));
	private final ImageIcon queenDiamonds = new ImageIcon(getClass().getResource("queen_of_diamonds2.png"));
	private final ImageIcon queenHearts = new ImageIcon(getClass().getResource("queen_of_hearts2.png"));
	private final ImageIcon queenSpades = new ImageIcon(getClass().getResource("queen_of_spades2.png"));
	public ImageIcon getTenClubs() {
		return tenClubs;
	}
	public ImageIcon getTenDiamonds() {
		return tenDiamonds;
	}
	public ImageIcon getTenHearts() {
		return tenHearts;
	}
	public ImageIcon getTenSpades() {
		return tenSpades;
	}
	public ImageIcon getTwoClubs() {
		return twoClubs;
	}
	public ImageIcon getTwoDiamonds() {
		return twoDiamonds;
	}
	public ImageIcon getTwoHearts() {
		return twoHearts;
	}
	public ImageIcon getTwoSpades() {
		return twoSpades;
	}
	public ImageIcon getThreeClubs() {
		return threeClubs;
	}
	public ImageIcon getThreeDiamonds() {
		return threeDiamonds;
	}
	public ImageIcon getThreeHearts() {
		return threeHearts;
	}
	public ImageIcon getThreeSpades() {
		return threeSpades;
	}
	public ImageIcon getFourClubs() {
		return fourClubs;
	}
	public ImageIcon getFourDiamonds() {
		return fourDiamonds;
	}
	public ImageIcon getFourHearts() {
		return fourHearts;
	}
	public ImageIcon getFourSpades() {
		return fourSpades;
	}
	public ImageIcon getFiveClubs() {
		return fiveClubs;
	}
	public ImageIcon getFiveDiamonds() {
		return fiveDiamonds;
	}
	public ImageIcon getFiveHearts() {
		return fiveHearts;
	}
	public ImageIcon getFiveSpades() {
		return fiveSpades;
	}
	public ImageIcon getSixClubs() {
		return sixClubs;
	}
	public ImageIcon getSixDiamonds() {
		return sixDiamonds;
	}
	public ImageIcon getSixHearts() {
		return sixHearts;
	}
	public ImageIcon getSixSpades() {
		return sixSpades;
	}
	public ImageIcon getSevenClubs() {
		return sevenClubs;
	}
	public ImageIcon getSevenDiamonds() {
		return sevenDiamonds;
	}
	public ImageIcon getSevemHearts() {
		return sevenHearts;
	}
	public ImageIcon getSevenSpades() {
		return sevenSpades;
	}
	public ImageIcon getEightClubs() {
		return eightClubs;
	}
	public ImageIcon getEightDiamonds() {
		return eightDiamonds;
	}
	public ImageIcon getEightHearts() {
		return eightHearts;
	}
	public ImageIcon getEightSpades() {
		return eightSpades;
	}
	public ImageIcon getNineClubs() {
		return nineClubs;
	}
	public ImageIcon getNineDiamonds() {
		return nineDiamonds;
	}
	public ImageIcon getNineHearts() {
		return nineHearts;
	}
	public ImageIcon getNineSpades() {
		return nineSpades;
	}
	public ImageIcon getAceClubs() {
		return aceClubs;
	}
	public ImageIcon getAceDiamonds() {
		return aceDiamonds;
	}
	public ImageIcon getAceHearts() {
		return aceHearts;
	}
	public ImageIcon getAceSpades() {
		return aceSpades;
	}
	public ImageIcon getBack() {
		return back;
	}
	public ImageIcon getBlank() {
		return blank;
	}
	public ImageIcon getJackClubs() {
		return jackClubs;
	}
	public ImageIcon getJackDiamonds() {
		return jackDiamonds;
	}
	public ImageIcon getJackHearts() {
		return jackHearts;
	}
	public ImageIcon getJackSpades() {
		return jackSpades;
	}
	public ImageIcon getKingClubs() {
		return kingClubs;
	}
	public ImageIcon getKingDiamonds() {
		return kingDiamonds;
	}
	public ImageIcon getKingHearts() {
		return kingHearts;
	}
	public ImageIcon getKingSpades() {
		return kingSpades;
	}
	public ImageIcon getQueenClubs() {
		return queenClubs;
	}
	public ImageIcon getQueenDiamonds() {
		return queenDiamonds;
	}
	public ImageIcon getQueenHearts() {
		return queenHearts;
	}
	public ImageIcon getQueenSpades() {
		return queenSpades;
	}
	
	public ImageIcon setImageOfCard(Card card){
		int suit = card.getSuit();
		int value = card.getValue();
		
		switch(value){
		case Constants.ACE:{
			switch(suit){
			case Constants.CLUBS: return aceClubs;
			case Constants.DIAMONDS: return aceDiamonds;
			case Constants.HEARTS: return aceHearts;
			case Constants.SPADES: return aceSpades;
			}
		}
		case 2:{
				switch(suit){
				case Constants.CLUBS: return twoClubs;
				case Constants.DIAMONDS: return twoDiamonds;
				case Constants.HEARTS: return twoHearts;
				case Constants.SPADES: return twoSpades;
				}
		}
		case 3:{
			switch(suit){
			case Constants.CLUBS: return threeClubs;
			case Constants.DIAMONDS: return threeDiamonds;
			case Constants.HEARTS: return threeHearts;
			case Constants.SPADES: return threeSpades;
			}
		}
		case 4:{
			switch(suit){
			case Constants.CLUBS: return fourClubs;
			case Constants.DIAMONDS: return fourDiamonds;
			case Constants.HEARTS: return fourHearts;
			case Constants.SPADES: return fourSpades;
			}
		}
		case 5:{
			switch(suit){
			case Constants.CLUBS: return fiveClubs;
			case Constants.DIAMONDS: return fiveDiamonds;
			case Constants.HEARTS: return fiveHearts;
			case Constants.SPADES: return fiveSpades;
			}
		}
		case 6:{
			switch(suit){
			case Constants.CLUBS: return sixClubs;
			case Constants.DIAMONDS: return sixDiamonds;
			case Constants.HEARTS: return sixHearts;
			case Constants.SPADES: return sixSpades;
			}
		}
		case 7:{
			switch(suit){
			case Constants.CLUBS: return sevenClubs;
			case Constants.DIAMONDS: return sevenDiamonds;
			case Constants.HEARTS: return sevenHearts;
			case Constants.SPADES: return sevenSpades;
			}
		}
		case 8:{
			switch(suit){
			case Constants.CLUBS: return eightClubs;
			case Constants.DIAMONDS: return eightDiamonds;
			case Constants.HEARTS: return eightHearts;
			case Constants.SPADES: return eightSpades;
			}
		}
		case 9:{
			switch(suit){
			case Constants.CLUBS: return nineClubs;
			case Constants.DIAMONDS: return nineDiamonds;
			case Constants.HEARTS: return nineHearts;
			case Constants.SPADES: return nineSpades;
			}
		}
		case 10:{
			switch(suit){
			case Constants.CLUBS: return tenClubs;
			case Constants.DIAMONDS: return tenDiamonds;
			case Constants.HEARTS: return tenHearts;
			case Constants.SPADES: return tenSpades;
			}
		}
		case Constants.JACK:{
			switch(suit){
			case Constants.CLUBS: return jackClubs;
			case Constants.DIAMONDS: return jackDiamonds;
			case Constants.HEARTS: return jackHearts;
			case Constants.SPADES: return jackSpades;
			}
		}
		case Constants.QUEEN:{
			switch(suit){
			case Constants.CLUBS: return queenClubs;
			case Constants.DIAMONDS: return queenDiamonds;
			case Constants.HEARTS: return queenHearts;
			case Constants.SPADES: return queenSpades;
			}
		}
		case Constants.KING:{
			switch(suit){
			case Constants.CLUBS: return kingClubs;
			case Constants.DIAMONDS: return kingDiamonds;
			case Constants.HEARTS: return kingHearts;
			case Constants.SPADES: return kingSpades;
			}
		}
		default:return aceHearts;
		}
		
		
	}
	
}
