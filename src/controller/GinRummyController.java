package controller;
import joc.Play;
import view.CardView;
import view.Table;
import javax.swing.*;

import carti.Deck;
import carti.Hand;

import java.awt.event.*;
public class GinRummyController {
	private Play play;
	private Table table;
	
	public GinRummyController(Play m_play , Table m_table){
		play = m_play;
		table = m_table;
		
		table.addCardsListener(new CardsListener());
		table.addDealListener(new DealListener());
		table.addDeckDiscardListener(new DeckDiscardListener());
		table.addSortListener(new SortListener());
	}
	
	class CardsListener implements MouseListener{
		public void mouseClicked(MouseEvent event){
			if (table.isButtonPermission()){
				int carteAleasa = 0;
				for (int i=0;i<11;i++)
					if (event.getSource() == table.getPlayerImage(i))
						carteAleasa = i;
			
			
				play.player1.discard(play.discardPile,carteAleasa);
				CardView cardView = new CardView();
				ImageIcon image = new ImageIcon();
				image = cardView.setImageOfCard(play.discardPile.getTop());
				table.setDiscardImage(image);
				for (int i=0;i<10;i++){
					image = cardView.setImageOfCard(play.player1.getHand().cardByPosition(i));
					table.setPlayerImage(image, i);
				}
				image = cardView.getBlank();
				table.setPlayerImage(image , 10);
				
				play.player2.draw(play.deck, play.discardPile , false);
				play.player2.pair();
				play.player2.setKnock(play.player2.canIKnock());
				if (!play.player2.getKnock()){
					play.player2.discard(play.discardPile , 0);
					image = cardView.setImageOfCard(play.discardPile.getTop());
					table.setDiscardImage(image);
					table.setHelpText("Va rugam trageti o carte");
				}
				else{
					play.addPoints();
					
					for (int i=0;i<play.player2.getHand().getSize();i++){
						image = cardView.setImageOfCard(play.player2.getHand().cardByPosition(i));
						table.setComputerImage(image, i);
					}
					play.player1.getHand().removeAllCards();
					play.player1.getHand().removeAllCards();
					if (play.player2.getPoints()<100){
						table.setAllowDeal(true);
						JOptionPane.showMessageDialog(null ,"Player1   " + "Computer" + "\n"+play.player1.getPoints() + "     "+play.player2.getPoints() ,"Calculatorul a castigat runda" , JOptionPane.PLAIN_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(null, "NE PARE RAU , DAR ATI PIERDUT.MULT NOROC DATA VIITOARE" , "",JOptionPane.PLAIN_MESSAGE);

				}
				
				
			table.setButtonPermission(false);
			}
		}
		public void mouseEntered(MouseEvent event){}
		public void mouseReleased(MouseEvent event){}
		public void mouseExited(MouseEvent event){}
		public void mousePressed(MouseEvent event){}
	}
	
	class DeckDiscardListener implements MouseListener{
		
		public void mouseClicked(MouseEvent event){
			if (!table.isButtonPermission()){
			boolean trageCarte = false;
			if (event.getSource() == table.getDeckImage())
				trageCarte = true;
			else if (event.getSource() == table.getDeckImage())
				trageCarte = false;
			
			
				play.player1.draw(play.deck, play.discardPile, trageCarte);
				CardView cardView = new CardView();
				ImageIcon image = new ImageIcon();
				image = cardView.setImageOfCard(play.player1.getHand().cardByPosition(10));
				table.setPlayerImage(image, 10);
				if (!trageCarte){
					image = cardView.getBlank();
					table.setDiscardImage(image);
				}
			play.player1.pair();
			play.player1.setKnock(play.player1.canIKnock());
			if (play.player1.getKnock()){
				for (int i=0;i<10;i++){
					image = cardView.setImageOfCard(play.player2.getHand().cardByPosition(i));
					table.setComputerImage(image, i);
				}
				play.addPoints();
				
				play.player1.getHand().removeAllCards();
				play.player2.getHand().removeAllCards();
				if (play.player1.getPoints() < 100){
					table.setAllowDeal(true);
					JOptionPane.showMessageDialog(null ,"Player1   " + "Computer" + "\n"+play.player1.getPoints() + "     "+play.player2.getPoints() ,"Felicitari.Ai castigat runda!" , JOptionPane.PLAIN_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "punctaj obtinut:"+play.player1.getPoints() , "FELICITARI!AI CASTIGAT JOCUL",JOptionPane.PLAIN_MESSAGE);
			}
			else
				table.setHelpText("Va rugam lasati o carte jos");
			table.setButtonPermission(true);
			}
		}
		public void mouseEntered(MouseEvent event){}
		public void mouseReleased(MouseEvent event){}
		public void mouseExited(MouseEvent event){}
		public void mousePressed(MouseEvent event){}
	}
	
	class DealListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (table.isAllowDeal()){
			play.deck = new Deck();
			Hand handP1 = new Hand();
			Hand handP2 = new Hand();
			for (int i=0;i<10;i++){
				
					handP1.addCard(play.deck.dealCard());
					handP2.addCard(play.deck.dealCard());
				
				
			}
			
				play.discardPile.setTop(play.deck.dealCard());
			
			play.player1.setHand(handP1);
			play.player2.setHand(handP2);
			play.player1.setKnock(false);
			play.player2.setKnock(false);
			CardView cardView = new CardView();
			ImageIcon image = new ImageIcon();
			for (int i=0;i<10;i++){
				image = cardView.setImageOfCard(play.player1.getHand().cardByPosition(i));
				table.setPlayerImage(image, i);
			}
			image = cardView.getBack();
			for (int i=0;i<10;i++)
				table.setComputerImage(image, i);
			image = cardView.getBlank();
			table.setPlayerImage(image, 10);
			table.setComputerImage(image, 10);
			image = cardView.setImageOfCard(play.discardPile.getTop());
			table.setDiscardImage(image);
			table.setHelpText("Va rugam trageti o carte");
			table.setButtonPermission(false);
		}
			table.setAllowDeal(false);
			
		}
	}
	
	class SortListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (event.getSource() == table.getSortByS()){
				play.player1.getHand().sortBySuit();
				CardView cardView = new CardView();
				ImageIcon image = new ImageIcon();
				for (int i=0;i<play.player1.getHand().getSize();i++){
					image = cardView.setImageOfCard(play.player1.getHand().cardByPosition(i));
					table.setPlayerImage(image, i);
				}
			}
			else if (event.getSource() == table.getSortByV()){
				play.player1.getHand().sortByValue();
				CardView cardView = new CardView();
				ImageIcon image = new ImageIcon();
				for (int i=0;i<play.player1.getHand().getSize();i++){
					image = cardView.setImageOfCard(play.player1.getHand().cardByPosition(i));
					table.setPlayerImage(image, i);
				}
			}
			
		}
	}
}
