package principal;

import controller.GinRummyController;
import joc.Play;
import view.Table;

public class GinRummy {
	public static void main(String[] args) throws Exception{
	Play play = new Play();
	Table table = new Table();
	GinRummyController controller = new GinRummyController(play , table);
	table.setVisible(true);
	}
}
